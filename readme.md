# Shortify plugin

## Overview
Shortens long list of items.

By default shows first 10 items and Show/Hide button.
#### HTML before activating shortify.
```html
  <ul class="list">
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
      <li><a href="#">6</a></li>
      <!--   ...    -->
      <li><a href="#">18</a></li>
      <li><a href="#">19</a></li>
      <li><a href="#">20</a></li>
    </ul>
```
#### HTML after shortify is activated.
```html
<ul class="list">
  <li><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li><a href="#">6</a></li>
  <li><a href="#">7</a></li>
  <li><a href="#">8</a></li>
  <li><a href="#">9</a></li>
  <li><a href="#">10</a></li>
  <li class="others" style="display: none;"><a href="#">11</a></li>
  <li class="others" style="display: none;"><a href="#">12</a></li>
  <li class="others" style="display: none;"><a href="#">13</a></li>
  <!--   ...    -->
  <li class="others" style="display: none;"><a href="#">19</a></li>
  <li class="others" style="display: none;"><a href="#">20</a></li>
</ul>
<a href="#" class="shortify-toggle closed">Show</a>
```
> Css class ``.others`` is appended to each element that is beyond defined range.

> Toggle link with ``.shortify-toggle`` is appended after selected element.

## Usage:

### Via Javascript
Enable manually with:
```javascript
  $('.list').shortify();
```

### Options via Javascript
```js
  $('.list').shortify({
    item: 'li',
    range: 20,
    buttonOpened: 'Hide items',
    buttonClosed: '<span>Show all items</span>',
    control_class: 'closed'
  });  
```

### Options

| Name          |type           |default        | description                                                                                                                         |
| ------------- |:-------------:|:-------------:| -------------                                                                                                                       |
| item          |selector       |li             | Items you want to hide                                                                                                              |
| range         |integer        |10             | Number of items which remain visible                                                                                                |
| buttonOpened  |string         |Hide           | Button text or html which is displayed when all items are visible                                                                   |
| buttonClosed  |string         |Show           | Button text or html which is displayed when items beyond range are hidden                                                           |
| control_class |string         |closed         | Css class which gets added to the ``.shortify toggle`` button. Indicates that shortify is showing only items which are within range |