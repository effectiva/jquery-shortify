$(function(){
  'use strict';

  var Shortify = function(el, opts){
    this.$el = $(el);
    this.opts = $.extend({
      item: 'li',
      range: 10
    }, opts || {});
    this.buttonOpened = this.opts.buttonOpened || 'Hide';
    this.buttonClosed = this.opts.buttonClosed || 'Show';
    this.buttonText = this.buttonClosed;
    this.control_class = this.opts.control_class || 'closed';
    this.$container = $('<div class="shortify"></div>');
    this.$toggle = $('<a href="#" class="shortify-toggle" alt="Toggle list items">'+this.buttonText+'</a>');
    this.setup_layout();
    this.setup_events();
  };

  Shortify.prototype.setup_layout = function() {
    this.$items = this.$el.find(this.opts.item + ':gt('+ (this.opts.range-1) +')');
    if(this.$items.length ===0) {return;}
    this.$items.addClass('others');
    this.$el.after(this.$toggle);
    this.$toggle.addClass(this.control_class);
    this.$items.hide();
  };

  Shortify.prototype.setup_events = function() {
    var self = this;
    this.$toggle.on('click', function(e){
      e.preventDefault();
      $(this).hasClass(self.control_class) ? self.show_items() : self.hide_items();
    });
  };

  Shortify.prototype.hide_items = function() {
    this.$items.hide(400);
    this.$toggle.addClass(this.control_class);
    this.update_button(this.buttonClosed);
  };

  Shortify.prototype.show_items = function() {
    this.$items.show(400);
    this.$toggle.removeClass(this.control_class);
    this.update_button(this.buttonOpened);
  };

  Shortify.prototype.update_button  = function(state) {
    this.$toggle.html(state);
  };

  //Expose as jquery plugin
  $.fn.shortify = function(options){
      var method = typeof options === 'string' && options;
      $(this).each(function(){
          var $this = $(this);
          var instance = $this.data('shortify');
          if(instance){
            method && instance[method]();
            return;
          }
          instance = new Shortify(this, options);
          $this.data('shortify', instance);
      });
      return this;
  };

  //Expose class
  $.Shortify = Shortify;

});
